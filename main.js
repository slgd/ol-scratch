import 'ol/ol.css';
import {Map, View} from 'ol';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import Feature from 'ol/Feature';
import Polygon, {circular} from 'ol/geom/Polygon';
import Style from "ol/style/Style";
import Stroke from "ol/style/Stroke";
import Fill from "ol/style/Fill";
import Draw from "ol/interaction/Draw";
import {transform} from "ol/proj";
import Circle from "ol/geom/Circle";
import {getDistance} from "ol/sphere";

import OLCesium from 'olcs/OLCesium';


const projection = "EPSG:4326";

const imageLayer = new TileLayer({
  source: new OSM(),
  zIndex: 0
});

const points1_4326 = [[-50, -10], [-50, 10], [-10, 10], [-10, -10], [-50, -10]];
const feature1 = new Feature({
  geometry: new Polygon([points1_4326.map(coord => transform(coord, "EPSG:4326", projection))]),
});
feature1.setStyle(new Style({
  stroke: new Stroke({
    color: 'red',
    width: 3
  }),
  fill: new Fill({
    color: 'rgba(255, 0, 0, 0.1)'
  }),
  zIndex: 3
}));

const points2_4326 = [[-20, -10], [-20, 10], [10, 10], [10, -10], [-20, -10]];
const feature2 = new Feature({
  geometry: new Polygon([points2_4326.map(coord => transform(coord, "EPSG:4326", projection))]),
});
feature2.setStyle(new Style({
  stroke: new Stroke({
    color: 'green',
    width: 3
  }),
  fill: new Fill({
    color: 'rgba(0, 255, 0, 0.1)'
  }),
  zIndex: 1
}));

const vectorLayer = new VectorLayer({
  source: new VectorSource({
    features: [feature1, feature2]
  }),
  zIndex: 1
});

const drawSource = new VectorSource({wrapX: false});
const drawLayer = new VectorLayer({source: drawSource});

const mapNode = document.getElementById('map');
while (mapNode.firstChild) {
  mapNode.removeChild(mapNode.firstChild);
}

const map = new Map({
  target: 'map',
  layers: [imageLayer, vectorLayer, drawLayer],
  view: new View({
    center: [0, 0],
    zoom: 2,
    projection: projection
  })
});

const geometryFunction = ([center, point], geometry) => {
  const c = transform(center, projection, "EPSG:4326");
  const p = transform(point, projection, "EPSG:4326");
  const distance = getDistance(c, p);
  const new_geometry = circular(c, distance, 512).transform("EPSG:4326", projection);

  if (geometry == null) {
    geometry = new_geometry;
  } else {
    geometry.setCoordinates(new_geometry.getCoordinates());
  }

  return geometry;
};

map.addInteraction(new Draw({
  source: drawSource,
  type: 'Circle',
  geometryFunction
}));


//const ol3d = new OLCesium({map: map});
//setTimeout(() => ol3d.setEnabled(true), 10000);
